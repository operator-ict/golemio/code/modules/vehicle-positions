import sinon, { SinonSandbox, SinonStub } from "sinon";
import { Validator } from "@golemio/core/dist/shared/golemio-validator";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import moment from "@golemio/core/dist/shared/moment-timezone";
import { VehiclePositions } from "#sch/index";
import { VehiclePositionsTransformation } from "#ie/VehiclePositionsTransformation";

chai.use(chaiAsPromised);

const readFile = (file: string): Promise<Buffer> => {
    return new Promise((resolve, reject) => {
        const stream = fs.createReadStream(file);
        const chunks: any[] = [];

        stream.on("error", (err) => {
            reject(err);
        });
        stream.on("data", (data) => {
            chunks.push(data);
        });
        stream.on("close", () => {
            resolve(Buffer.concat(chunks));
        });
    });
};

describe("VehiclePositionsTransformation", () => {
    let transformation: VehiclePositionsTransformation;
    let testSourceData: Record<string, any>;
    let positionsValidator: Validator;
    let stopsValidator: Validator;
    let tripsValidator: Validator;
    let testSourceDataTimestamps: any;

    before(() => {
        positionsValidator = new Validator(
            VehiclePositions.positions.name + "ModelValidator",
            VehiclePositions.positions.outputMongooseSchemaObject
        );
        stopsValidator = new Validator(
            VehiclePositions.positions.name + "ModelValidator",
            VehiclePositions.positions.outputMongooseSchemaObject
        );
        tripsValidator = new Validator(
            VehiclePositions.trips.name + "ModelValidator",
            VehiclePositions.trips.outputMongooseSchemaObject
        );
        testSourceDataTimestamps = JSON.parse(
            fs.readFileSync(__dirname + "/./data/vehiclepositions-input-timestamps.json").toString("utf8")
        );
    });

    beforeEach(async () => {
        transformation = new VehiclePositionsTransformation();
        const buffer = await readFile(__dirname + "/./data/vehiclepositions-input.json");
        testSourceData = JSON.parse(Buffer.from(buffer).toString("utf8"));
    });

    it("should has name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("VehiclePositions");
    });

    it("should has transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform element (negative bearing)", async () => {
        const data = await transformation.transform(testSourceData.m.spoj[0]);
        expect(data).to.have.property("positions");
        expect(data.positions[0]).to.have.property("lat", 50.16252);
        expect(data.positions[0]).to.have.property("lng", 14.52483);
        expect(data.positions[0]).to.have.property("bearing", -10 + 256);
        expect(data).to.have.property("stops");
        expect(data.stops.length).to.equal(28);
        expect(data).to.have.property("trips");
        expect(data.trips[0]).to.have.property("cis_line_id", "100110");
    });

    it("should properly transform element (positive bearing)", async () => {
        const data = await transformation.transform(testSourceData.m.spoj[1]);
        expect(data).to.have.property("positions");
        expect(data.positions[0]).to.have.property("lat", 50.1053);
        expect(data.positions[0]).to.have.property("lng", 14.54619);
        expect(data.positions[0]).to.have.property("bearing", 114);
        expect(data).to.have.property("stops");
        expect(data.stops.length).to.equal(24);
        expect(data).to.have.property("trips");
        expect(data.trips[0]).to.have.property("cis_line_id", "100110");
    });

    it("should properly transform DPP stop id (to AWS id)", async () => {
        const data = await transformation.transform(testSourceData.m.spoj[2]);
        expect(data.trips[0]).to.have.property("agency_name_real", "DP PRAHA");
        expect(data.trips[0]).to.have.property("start_asw_stop_id", "628/4");
        expect(data).to.have.property("positions");
        // expect(data.positions[0]).to.have.property("asw_last_stop_id", "628/4");
        expect(data).to.have.property("stops");
        expect(data.stops.length).to.equal(2);
        expect(data.stops[0]).to.have.property("asw_stop_id", "628/4");
        expect(data.stops[1]).to.have.property("asw_stop_id", "1090/3");
    });

    it("should properly deduce timestamps (normal situation))", async () => {
        sinon.useFakeTimers({
            now: moment("2021-03-16T11:58:59+01:00").toDate(),
            shouldAdvanceTime: true,
        });
        // @ts-ignore
        const data = await transformation.deduceTimestamps(
            testSourceDataTimestamps.m.spoj[0].zast[0],
            testSourceDataTimestamps.m.spoj[0].$.cpoz
        );
        // @ts-ignore
        expect(moment("2021-03-16T11:15:00+01:00").isSame(data.startTimestamp)).to.equal(true);
        expect(moment("2021-03-16T11:58:57+01:00").isSame(data.positionTimestamp)).to.equal(true);
    });

    it("should properly deduce timestamps (23.45 starts, 23.59 sends position, 0.00 is now))", async () => {
        sinon.useFakeTimers({
            now: moment("2021-03-17T00:00:10+01:00").toDate(),
            shouldAdvanceTime: true,
        });
        // @ts-ignore
        const data = await transformation.deduceTimestamps(
            testSourceDataTimestamps.m.spoj[1].zast[0],
            testSourceDataTimestamps.m.spoj[1].$.cpoz
        );
        // @ts-ignore
        expect(moment("2021-03-16T23:45:00+01:00").isSame(data.startTimestamp)).to.equal(true);
        expect(moment("2021-03-16T23:59:50+01:00").isSame(data.positionTimestamp)).to.equal(true);
    });

    it("should properly deduce timestamps (23.45 starts, 0.00 sends position, 0.00 is now))", async () => {
        sinon.useFakeTimers({
            now: moment("2021-03-17T00:00:15+01:00").toDate(),
            shouldAdvanceTime: true,
        });
        // @ts-ignore
        const data = await transformation.deduceTimestamps(
            testSourceDataTimestamps.m.spoj[2].zast[0],
            testSourceDataTimestamps.m.spoj[2].$.cpoz
        );
        // @ts-ignore
        expect(moment("2021-03-16T23:45:00+01:00").isSame(data.startTimestamp)).to.equal(true);
        expect(moment("2021-03-17T00:00:10+01:00").isSame(data.positionTimestamp)).to.equal(true);
    });

    it("should properly deduce timestamps (23.45+1 starts, 3.10+2 sends position, 1.10Z is now)", async () => {
        sinon.useFakeTimers({
            now: moment("2021-03-28T01:10:15+00:00").toDate(),
            shouldAdvanceTime: true,
        });
        // @ts-ignore
        const data = await transformation.deduceTimestamps(
            testSourceDataTimestamps.m.spoj[3].zast[0],
            testSourceDataTimestamps.m.spoj[3].$.cpoz
        );
        // @ts-ignore
        expect(moment("2021-03-27T23:45:00+01:00").isSame(data.startTimestamp)).to.equal(true);
        expect(moment("2021-03-28T03:10:10+02:00").isSame(data.positionTimestamp)).to.equal(true);
    });

    it("should properly deduce timestamps (1.00+1 starts, 3.10+2 sends position, 1.10Z is now)", async () => {
        sinon.useFakeTimers({
            now: moment("2021-03-28T01:10:15+00:00").toDate(),
            shouldAdvanceTime: true,
        });
        // @ts-ignore
        const data = await transformation.deduceTimestamps(
            testSourceDataTimestamps.m.spoj[4].zast[0],
            testSourceDataTimestamps.m.spoj[4].$.cpoz
        );
        // @ts-ignore
        expect(moment("2021-03-28T01:00:00+01:00").isSame(data.startTimestamp)).to.equal(true);
        expect(moment("2021-03-28T03:10:10+02:00").isSame(data.positionTimestamp)).to.equal(true);
    });

    it("should properly deduce timestamps (3.10+2 starts, 3.30+2 sends position, 1.10Z is now)", async () => {
        sinon.useFakeTimers({
            now: moment("2021-03-28T01:30:15+00:00").toDate(),
            shouldAdvanceTime: true,
        });
        // @ts-ignore
        const data = await transformation.deduceTimestamps(
            testSourceDataTimestamps.m.spoj[5].zast[0],
            testSourceDataTimestamps.m.spoj[5].$.cpoz
        );
        // @ts-ignore
        expect(moment("2021-03-28T03:10:00+02:00").isSame(data.startTimestamp)).to.equal(true);
        expect(moment("2021-03-28T03:30:10+02:00").isSame(data.positionTimestamp)).to.equal(true);
    });

    it("should properly transform element (with old zpoz_typ attribute)", async () => {
        const data = await transformation.transform(testSourceData.m.spoj[0]);
        expect(data).to.have.property("stops");
        expect(data.stops[0]).to.have.property("delay_type", 3);
        expect(data.stops[16]).to.have.property("delay_type", 2);
    });

    it("should properly transform element (with new zpoz_typ_odj attribute)", async () => {
        const buffer = await readFile(__dirname + "/./data/vehiclepositions-input-zpoz_typ_odj.json");
        const testSourceDataNew = JSON.parse(Buffer.from(buffer).toString("utf8"));
        const data = await transformation.transform(testSourceDataNew.m.spoj[0]);
        expect(data).to.have.property("stops");
        expect(data.stops[0]).to.have.property("delay_type", 3);
        expect(data.stops[16]).to.have.property("delay_type", 2);
    });

    it("should set null for registration number, and others for DP PRAHA vehicles exluding trams)", async () => {
        const buffer = await readFile(__dirname + "/./data/vehiclepositions-input-registrationnumber.json");
        let testSourceDataRN = JSON.parse(Buffer.from(buffer).toString("utf8"));

        const data = await transformation.transform(testSourceDataRN.m.spoj[0]);
        expect(data.trips[0]).to.have.property("agency_name_real", "DP PRAHA");
        expect(data.trips[0]).to.have.property("vehicle_registration_number", null);
        expect(data.positions[0]).to.have.property("bearing", null);
        expect(data.positions[0]).to.have.property("speed", null);
    });

    it("should NOT set null for registration number for DP PRAHA trams)", async () => {
        const buffer = await readFile(__dirname + "/./data/vehiclepositions-input-registrationnumber.json");
        let testSourceDataRN = JSON.parse(Buffer.from(buffer).toString("utf8"));

        const data = await transformation.transform(testSourceDataRN.m.spoj[1]);
        expect(data.trips[0]).to.have.property("agency_name_real", "DP PRAHA");
        expect(data.trips[0]).to.have.property("vehicle_registration_number", 9428);
    });

    it("should set full atributes for registration number for Jaroslav Štěpánek vehicles)", async () => {
        const buffer = await readFile(__dirname + "/./data/vehiclepositions-input-registrationnumber.json");
        let testSourceDataRN = JSON.parse(Buffer.from(buffer).toString("utf8"));

        const data = await transformation.transform(testSourceDataRN.m.spoj[2]);
        expect(data.trips[0]).to.have.property("agency_name_real", "Jaroslav Štěpánek");
        expect(data.trips[0]).to.have.property("vehicle_registration_number", 1030);
        expect(data.positions[0]).to.have.property("bearing", 246);
        expect(data.positions[0]).to.have.property("speed", 0);
    });
});
