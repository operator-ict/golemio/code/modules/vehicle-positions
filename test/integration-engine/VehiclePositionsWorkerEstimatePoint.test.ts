import sinon, { SinonFakeTimers, SinonSandbox, SinonStub } from "sinon";
import { PostgresConnector, RedisConnector } from "@golemio/core/dist/integration-engine/connectors";
import { VehiclePositionsWorker } from "#ie/VehiclePositionsWorker";
import chai, { expect } from "chai";
import { config } from "@golemio/core/dist/integration-engine/config";
import moment from "@golemio/core/dist/shared/moment-timezone";
import fs from "fs";

describe("VehiclePositionsWorkerEstimatePoint", () => {
    let worker: VehiclePositionsWorker;
    let sequelizeModelStub: Record<string, SinonStub>;
    let sandbox: SinonSandbox;
    let clock: SinonFakeTimers;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        clock = sinon.useFakeTimers({
            now: new Date(2021, 2, 17, 10, 0),
            shouldAdvanceTime: true,
        });
        sequelizeModelStub = Object.assign({
            belongsTo: sandbox.stub(),
            hasMany: sandbox.stub(),
            hasOne: sandbox.stub(),
            removeAttribute: sandbox.stub(),
        });
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().callsFake(() => sequelizeModelStub),
                query: sandbox.stub().callsFake(() => [true]),
                transaction: sandbox.stub().callsFake(() => Object.assign({ commit: sandbox.stub() })),
            })
        );
        sandbox.stub(RedisConnector, "getConnection");

        worker = new VehiclePositionsWorker();

        sandbox
            .stub(worker["modelGTFSTrips"], "findByIdWithStopTimes")
            .callsFake(() => JSON.parse(fs.readFileSync(__dirname + "/./data/delay-stoptimes-01.json").toString()));
        sandbox
            .stub(worker["modelGTFSTrips"], "findByIdWithShapes")
            .callsFake(() => JSON.parse(fs.readFileSync(__dirname + "/./data/delay-shapes-01.json").toString()));

        const step = 0.05;
        sandbox.stub(config.vehiclePositions, "stepBetweenPoints").value(step);
    });

    afterEach(() => {
        sandbox.restore();
        clock.restore();
    });

    it("getStopTimesForDelayComputation should return proper data", async () => {
        const stopTimes = await worker["modelGTFSTrips"]["findByIdWithStopTimes"]("");
        const shapes = await worker["modelGTFSTrips"]["findByIdWithShapes"]("");

        const trip = {
            ...stopTimes,
            ...shapes,
        };

        expect(await worker["getStopTimesForDelayComputation"](trip)).to.deep.equal(
            JSON.parse(fs.readFileSync(__dirname + "/./data/delay-tripstoptimes-01.json").toString())
        );
    });

    it("getShapesAnchorPointsForDelayComputation should return proper data", async () => {
        const stopTimes = await worker["modelGTFSTrips"]["findByIdWithStopTimes"]("");
        const shapes = await worker["modelGTFSTrips"]["findByIdWithShapes"]("");

        const trip = {
            ...stopTimes,
            ...shapes,
        };

        expect(await worker["getShapesAnchorPointsForDelayComputation"](trip.shape_id, trip.shapes)).to.deep.equal(
            JSON.parse(fs.readFileSync(__dirname + "/./data/delay-shapeanchor-01.json").toString())
        );
    });

    it("getStartDayTimestamp should return same day", async () => {
        expect(
            await worker["getStartDayTimestamp"](moment("2021-03-17T00:00:00+01:00").valueOf(), 14 * 3600 + 10 * 60 + 0 * 1)
        ).to.equal(moment("2021-03-17T00:00:00+01:00").valueOf());
    });

    it("getStartDayTimestamp should return previous day", async () => {
        expect(
            await worker["getStartDayTimestamp"](moment("2021-03-17T00:00:00+01:00").valueOf(), 31 * 3600 + 10 * 60 + 0 * 1)
        ).to.equal(moment("2021-03-16T00:00:00+01:00").valueOf());
    });

    it("getStartDayTimestamp should return previous day CET from CEST", async () => {
        expect(
            await worker["getStartDayTimestamp"](moment("2021-03-29T04:00:00+02:00").valueOf(), 28 * 3600 + 10 * 60 + 0 * 1)
        ).to.equal(moment("2021-03-28T00:00:00+01:00").valueOf());
    });

    it("getResultObjectForDelayCalculation should return proper data", async () => {
        expect(await worker["getResultObjectForDelayCalculation"]("")).to.deep.equal(
            JSON.parse(fs.readFileSync(__dirname + "/./data/delay-redisobject-01.json").toString())
        );
    });

    it("getTimestampOffset should return 0 minutes offset", async () => {
        expect(
            worker["getTimezoneOffsetSeconds"](
                moment("2021-03-27T01:00:00+01:00").valueOf(),
                moment("2021-03-27T05:00:00+01:00").valueOf()
            )
        ).to.deep.equal(0 * 60);
    });

    it("getTimestampOffset should return -60 minutes offset", async () => {
        expect(
            worker["getTimezoneOffsetSeconds"](
                moment("2021-03-28T01:00:00+01:00").valueOf(),
                moment("2021-03-28T05:00:00+02:00").valueOf()
            )
        ).to.deep.equal(-60 * 60);
    });

    it("getTimestampOffset should return 0 minutes offset", async () => {
        expect(
            worker["getTimezoneOffsetSeconds"](
                moment("2021-10-31T01:00:00+02:00").valueOf(),
                moment("2021-10-31T01:30:00+02:00").valueOf()
            )
        ).to.deep.equal(0 * 60);
    });

    it("getTimestampOffset should return -60 minutes offset", async () => {
        expect(
            await worker["getTimezoneOffsetSeconds"](
                moment("2021-10-31T01:00:00+02:00").valueOf(),
                moment("2021-10-31T05:00:00+01:00").valueOf()
            )
        ).to.deep.equal(60 * 60);
    });

    it("getLocalTimeToUnix test - normal day in 12:15", async () => {
        expect(
            await worker["getLocalTimeToUnix"](moment("2021-03-27T00:00:00+01:00").valueOf(), 12 * 3600 + 15 * 60 + 0)
        ).to.equal(moment("2021-03-27T12:15:00+01:00").valueOf());
    });

    it("getLocalTimeToUnix test - saturday trip in sunday in 1:15", async () => {
        expect(
            await worker["getLocalTimeToUnix"](moment("2021-03-27T00:00:00+01:00").valueOf(), 25 * 3600 + 15 * 60 + 0)
        ).to.equal(moment("2021-03-28T01:15:00+01:00").valueOf());
    });

    it("getLocalTimeToUnix test - saturday trip in sunday in 3:15 (CET to CEST)", async () => {
        expect(
            await worker["getLocalTimeToUnix"](moment("2021-03-27T00:00:00+01:00").valueOf(), 27 * 3600 + 15 * 60 + 0)
        ).to.equal(moment("2021-03-28T03:15:00+02:00").valueOf());
    });

    it("getLocalTimeToUnix test - sunday trip in monday in 3:15 (CET to CEST)", async () => {
        expect(
            await worker["getLocalTimeToUnix"](moment("2021-03-28T00:00:00+01:00").valueOf(), 27 * 3600 + 15 * 60 + 0)
        ).to.equal(moment("2021-03-29T03:15:00+02:00").valueOf());
    });

    it("getLocalTimeToUnix test - sunday trip in sunday in 5:15 (CEST)", async () => {
        expect(
            await worker["getLocalTimeToUnix"](moment("2021-03-28T00:00:00+01:00").valueOf(), 5 * 3600 + 15 * 60 + 0)
        ).to.equal(moment("2021-03-28T05:15:00+02:00").valueOf());
    });

    it("getLocalTimeToUnix test - sunday trip in monday in 1:15 (CET)", async () => {
        expect(
            await worker["getLocalTimeToUnix"](moment("2021-10-31T00:00:00+02:00").valueOf(), 25 * 3600 + 15 * 60 + 0)
        ).to.equal(moment("2021-11-01T01:15:00+01:00").valueOf());
    });

    it("getEstimatedPoint should return for current position only on track", async () => {
        // position on track
        const currentPosition = {
            geometry: {
                coordinates: [14.4747, 50.1173],
                type: "Point",
            },
            properties: {
                origin_time: "10:00:10",
                origin_timestamp: moment("2021-03-17T10:00:10+01:00").valueOf(), //1615971610000
            },
            type: "Feature",
        };
        const lastPosition = null;
        const startTimestamp = moment("2021-03-17T10:00:00+01:00").valueOf();
        const shapePoints = JSON.parse(fs.readFileSync(__dirname + "/./data/delay-redisobject-01.json").toString()).shape_points;

        const startDayTimestamp = await worker["getStartDayTimestamp"](startTimestamp, shapePoints[0].time_scheduled_seconds);
        const estimatedPosition = await worker["getEstimatedPoint"](
            shapePoints,
            currentPosition,
            lastPosition,
            startTimestamp,
            startDayTimestamp
        );

        expect(estimatedPosition.properties.assigningProcess).to.equal("time-closest from start - options: 1");
        expect(estimatedPosition.geometry.coordinates).to.deep.equal([14.47485, 50.1173]);
    });

    it("getEstimatedPoint should return not found for gps too far (300m)", async () => {
        // position 350m far south
        const currentPosition = {
            geometry: {
                coordinates: [14.4759, 50.1139],
                type: "Point",
            },
            properties: {
                origin_time: "10:00:10",
                origin_timestamp: moment("2021-03-17T10:00:10+01:00").valueOf(), //1615971610000
            },
            type: "Feature",
        };
        const lastPosition = null;
        const startTimestamp = moment("2021-03-17T10:00:00+01:00").valueOf();
        const shapePoints = JSON.parse(fs.readFileSync(__dirname + "/./data/delay-redisobject-01.json").toString()).shape_points;

        const startDayTimestamp = await worker["getStartDayTimestamp"](startTimestamp, shapePoints[0].time_scheduled_seconds);
        const estimatedPosition = await worker["getEstimatedPoint"](
            shapePoints,
            currentPosition,
            lastPosition,
            startTimestamp,
            startDayTimestamp
        );

        expect(estimatedPosition.properties.assigningProcess).to.equal("not found");
        expect(estimatedPosition.geometry.coordinates).to.deep.equal([14.4759, 50.1139]);
    });

    it("getEstimatedPoint should return first from two possible", async () => {
        // position between two shape anchor points south, time is closer to first anchor
        const currentPosition = {
            geometry: {
                coordinates: [14.4676, 50.117],
                type: "Point",
            },
            properties: {
                origin_time: "10:00:10",
                origin_timestamp: moment("2021-03-17T10:00:10+01:00").valueOf(), //1615971610000
            },
            type: "Feature",
        };
        const lastPosition = null;
        const startTimestamp = moment("2021-03-17T10:00:00+01:00").valueOf();
        const shapePoints = JSON.parse(fs.readFileSync(__dirname + "/./data/delay-redisobject-01.json").toString()).shape_points;

        const startDayTimestamp = await worker["getStartDayTimestamp"](startTimestamp, shapePoints[0].time_scheduled_seconds);
        const estimatedPosition = await worker["getEstimatedPoint"](
            shapePoints,
            currentPosition,
            lastPosition,
            startTimestamp,
            startDayTimestamp
        );

        expect(estimatedPosition.properties.assigningProcess).to.equal("time-closest from start - options: 2");
        expect(estimatedPosition.geometry.coordinates).to.deep.equal([14.4686, 50.11709]);
    });

    it("getEstimatedPoint should return second from two possible", async () => {
        // position between two shape anchor points south, time is closer to second anchor
        const currentPosition = {
            geometry: {
                coordinates: [14.4676, 50.117],
                type: "Point",
            },
            properties: {
                origin_time: "10:20:10",
                origin_timestamp: moment("2021-03-17T10:20:10+01:00").valueOf(), //1615971610000
            },
            type: "Feature",
        };
        const lastPosition = null;
        const startTimestamp = moment("2021-03-17T10:00:00+01:00").valueOf();
        const shapePoints = JSON.parse(fs.readFileSync(__dirname + "/./data/delay-redisobject-01.json").toString()).shape_points;

        // @ts-ignore
        const startDayTimestamp = await worker.getStartDayTimestamp(startTimestamp, shapePoints[0].time_scheduled_seconds);
        // @ts-ignore
        const estimatedPosition = await worker.getEstimatedPoint(
            shapePoints,
            currentPosition,
            lastPosition,
            startTimestamp,
            startDayTimestamp
        );

        expect(estimatedPosition.properties.assigningProcess).to.equal("time-closest from start - options: 2");
        expect(estimatedPosition.geometry.coordinates).to.deep.equal([14.46714, 50.11778]);
    });

    it("getEstimatedPoint should return proper delay (72s delayed)", async () => {
        // position on track
        const currentPosition = {
            geometry: {
                coordinates: [14.4747, 50.1173],
                type: "Point",
            },
            properties: {
                origin_time: "10:01:41",
                origin_timestamp: moment("2021-03-17T10:01:41+01:00").valueOf(), //1615971610000
            },
            type: "Feature",
        };
        const lastPosition = null;
        const startTimestamp = moment("2021-03-17T10:00:00+01:00").valueOf();
        const shapePoints = JSON.parse(fs.readFileSync(__dirname + "/./data/delay-redisobject-01.json").toString()).shape_points;

        const startDayTimestamp = await worker["getStartDayTimestamp"](startTimestamp, shapePoints[0].time_scheduled_seconds);
        const estimatedPosition = await worker["getEstimatedPoint"](
            shapePoints,
            currentPosition,
            lastPosition,
            startTimestamp,
            startDayTimestamp
        );

        expect(estimatedPosition.properties.assigningProcess).to.equal("time-closest from start - options: 1");
        expect(estimatedPosition.geometry.coordinates).to.deep.equal([14.47485, 50.1173]);
        expect(estimatedPosition.properties.time_delay).to.equal(72);
    });
});
