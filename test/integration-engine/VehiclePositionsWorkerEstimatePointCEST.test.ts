import sinon, { SinonFakeTimers, SinonSandbox, SinonStub } from "sinon";
import { PostgresConnector, RedisConnector } from "@golemio/core/dist/integration-engine/connectors";
import { VehiclePositionsWorker } from "#ie/VehiclePositionsWorker";
import chai, { expect } from "chai";
import moment from "@golemio/core/dist/shared/moment-timezone";
import { config } from "@golemio/core/dist/integration-engine/config";
import fs from "fs";

describe("VehiclePositionsWorkerEstimatePointCEST", () => {
    let worker: VehiclePositionsWorker;
    let sequelizeModelStub: Record<string, SinonStub>;
    let sandbox: SinonSandbox;
    let clock: SinonFakeTimers;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        clock = sinon.useFakeTimers({
            now: new Date(2021, 2, 17, 10, 0),
            shouldAdvanceTime: true,
        });
        sequelizeModelStub = Object.assign({
            belongsTo: sandbox.stub(),
            hasMany: sandbox.stub(),
            hasOne: sandbox.stub(),
            removeAttribute: sandbox.stub(),
        });
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().callsFake(() => sequelizeModelStub),
                query: sandbox.stub().callsFake(() => [true]),
                transaction: sandbox.stub().callsFake(() => Object.assign({ commit: sandbox.stub() })),
            })
        );
        sandbox.stub(RedisConnector, "getConnection");

        worker = new VehiclePositionsWorker();

        const step = 0.05;
        sandbox.stub(config.vehiclePositions, "stepBetweenPoints").value(step);
    });

    afterEach(() => {
        sandbox.restore();
        clock.restore();
    });

    it("getEstimatedPoint should return proper delay (72s delayed)", async () => {
        // position on track
        const currentPosition = {
            geometry: {
                coordinates: [14.4747, 50.1173],
                type: "Point",
            },
            properties: {
                origin_time: "10:01:41",
                origin_timestamp: moment("2021-03-29T10:01:41+02:00").valueOf(),
            },
            type: "Feature",
        };
        const lastPosition = null;
        const startTimestamp = moment("2021-03-29T10:00:00+02:00").valueOf();

        // @ts-ignore
        sandbox
            .stub(worker["modelGTFSTrips"], "findByIdWithStopTimes")
            .callsFake(() => JSON.parse(fs.readFileSync(__dirname + "/./data/delay-stoptimes-01.json").toString()));
        // @ts-ignore
        sandbox
            .stub(worker["modelGTFSTrips"], "findByIdWithShapes")
            .callsFake(() => JSON.parse(fs.readFileSync(__dirname + "/./data/delay-shapes-01.json").toString()));
        // @ts-ignore
        const shapePoints = (await worker.getResultObjectForDelayCalculation("")).shape_points;

        // @ts-ignore
        const startDayTimestamp = await worker.getStartDayTimestamp(startTimestamp, shapePoints[0].time_scheduled_seconds);
        // @ts-ignore
        const estimatedPosition = await worker.getEstimatedPoint(
            shapePoints,
            currentPosition,
            lastPosition,
            startTimestamp,
            startDayTimestamp
        );

        expect(estimatedPosition.properties.assigningProcess).to.equal("time-closest from start - options: 1");
        expect(estimatedPosition.geometry.coordinates).to.deep.equal([14.47485, 50.1173]);
        expect(estimatedPosition.properties.time_delay).to.equal(72);
    });

    // eslint-disable-next-line max-len
    it("getEstimatedPoint should return proper delay (trip started day before, 72s delayed)", async () => {
        // position on track
        const currentPosition = {
            geometry: {
                coordinates: [14.4747, 50.1173],
                type: "Point",
            },
            properties: {
                origin_time: "04:01:41",
                origin_timestamp: moment("2021-03-27T04:01:41+01:00").valueOf(), //1615971610000
            },
            type: "Feature",
        };
        const lastPosition = null;
        const startTimestamp = moment("2021-03-27T04:00:00+01:00").valueOf();

        // @ts-ignore
        sandbox
            .stub(worker["modelGTFSTrips"], "findByIdWithStopTimes")
            .callsFake(() => JSON.parse(fs.readFileSync(__dirname + "/./data/delay-stoptimes-02.json").toString()));
        // @ts-ignore
        sandbox
            .stub(worker["modelGTFSTrips"], "findByIdWithShapes")
            .callsFake(() => JSON.parse(fs.readFileSync(__dirname + "/./data/delay-shapes-01.json").toString()));
        // @ts-ignore
        const shapePoints = (await worker.getResultObjectForDelayCalculation("")).shape_points;

        // @ts-ignore
        const startDayTimestamp = await worker.getStartDayTimestamp(startTimestamp, shapePoints[0].time_scheduled_seconds);

        // @ts-ignore
        const estimatedPosition = await worker.getEstimatedPoint(
            shapePoints,
            currentPosition,
            lastPosition,
            startTimestamp,
            startDayTimestamp
        );

        expect(estimatedPosition.properties.assigningProcess).to.equal("time-closest from start - options: 1");
        expect(estimatedPosition.geometry.coordinates).to.deep.equal([14.47485, 50.1173]);
        expect(estimatedPosition.properties.time_delay).to.equal(72);
    });

    it("getEstimatedPoint should return proper delay (72s delayed, even this day was shift from CET to CEST)", async () => {
        // position on track
        const currentPosition = {
            geometry: {
                coordinates: [14.4747, 50.1173],
                type: "Point",
            },
            properties: {
                origin_time: "10:01:41",
                origin_timestamp: moment("2021-03-28T10:01:41+02:00").valueOf(),
            },
            type: "Feature",
        };
        const lastPosition = null;
        const startTimestamp = moment("2021-03-28T10:00:00+02:00").valueOf();

        // @ts-ignore
        sandbox
            .stub(worker["modelGTFSTrips"], "findByIdWithStopTimes")
            .callsFake(() => JSON.parse(fs.readFileSync(__dirname + "/./data/delay-stoptimes-01.json").toString()));
        // @ts-ignore
        sandbox
            .stub(worker["modelGTFSTrips"], "findByIdWithShapes")
            .callsFake(() => JSON.parse(fs.readFileSync(__dirname + "/./data/delay-shapes-01.json").toString()));
        // @ts-ignore
        const shapePoints = (await worker.getResultObjectForDelayCalculation("")).shape_points;

        // @ts-ignore
        const startDayTimestamp = await worker.getStartDayTimestamp(startTimestamp, shapePoints[0].time_scheduled_seconds);
        // @ts-ignore
        const estimatedPosition = await worker.getEstimatedPoint(
            shapePoints,
            currentPosition,
            lastPosition,
            startTimestamp,
            startDayTimestamp
        );

        expect(estimatedPosition.properties.assigningProcess).to.equal("time-closest from start - options: 1");
        expect(estimatedPosition.geometry.coordinates).to.deep.equal([14.47485, 50.1173]);
        expect(estimatedPosition.properties.time_delay).to.equal(72);
    });

    // eslint-disable-next-line max-len
    it("getEstimatedPoint should return proper delay (trip started day before, 72s delayed, even this day was shift from CET to CEST)", async () => {
        // position on track
        const currentPosition = {
            geometry: {
                coordinates: [14.4747, 50.1173],
                type: "Point",
            },
            properties: {
                origin_time: "04:01:41",
                origin_timestamp: moment("2021-03-28T04:01:41+02:00").valueOf(), //1615971610000
            },
            type: "Feature",
        };
        const lastPosition = null;
        const startTimestamp = moment("2021-03-28T04:00:00+02:00").valueOf();

        // @ts-ignore
        sandbox
            .stub(worker["modelGTFSTrips"], "findByIdWithStopTimes")
            .callsFake(() => JSON.parse(fs.readFileSync(__dirname + "/./data/delay-stoptimes-02.json").toString()));
        // @ts-ignore
        sandbox
            .stub(worker["modelGTFSTrips"], "findByIdWithShapes")
            .callsFake(() => JSON.parse(fs.readFileSync(__dirname + "/./data/delay-shapes-01.json").toString()));
        // @ts-ignore
        const shapePoints = (await worker.getResultObjectForDelayCalculation("")).shape_points;

        // @ts-ignore
        const startDayTimestamp = await worker.getStartDayTimestamp(startTimestamp, shapePoints[0].time_scheduled_seconds);

        // @ts-ignore
        const estimatedPosition = await worker.getEstimatedPoint(
            shapePoints,
            currentPosition,
            lastPosition,
            startTimestamp,
            startDayTimestamp
        );

        expect(estimatedPosition.properties.assigningProcess).to.equal("time-closest from start - options: 1");
        expect(estimatedPosition.geometry.coordinates).to.deep.equal([14.47485, 50.1173]);
        expect(estimatedPosition.properties.time_delay).to.equal(72);
    });

    // eslint-disable-next-line max-len
    it("getEstimatedPoint should return proper delay (trip started day before, 72s delayed, even previous day was shift from CET to CEST)", async () => {
        // position on track
        const currentPosition = {
            geometry: {
                coordinates: [14.4747, 50.1173],
                type: "Point",
            },
            properties: {
                origin_time: "04:01:41",
                origin_timestamp: moment("2021-03-29T04:01:41+02:00").valueOf(), //1615971610000
            },
            type: "Feature",
        };
        const lastPosition = null;
        const startTimestamp = moment("2021-03-29T04:00:00+02:00").valueOf();

        // @ts-ignore
        sandbox
            .stub(worker["modelGTFSTrips"], "findByIdWithStopTimes")
            .callsFake(() => JSON.parse(fs.readFileSync(__dirname + "/./data/delay-stoptimes-02.json").toString()));
        // @ts-ignore
        sandbox
            .stub(worker["modelGTFSTrips"], "findByIdWithShapes")
            .callsFake(() => JSON.parse(fs.readFileSync(__dirname + "/./data/delay-shapes-01.json").toString()));
        // @ts-ignore
        const shapePoints = (await worker.getResultObjectForDelayCalculation("")).shape_points;

        // @ts-ignore
        const startDayTimestamp = await worker.getStartDayTimestamp(startTimestamp, shapePoints[0].time_scheduled_seconds);

        // @ts-ignore
        const estimatedPosition = await worker.getEstimatedPoint(
            shapePoints,
            currentPosition,
            lastPosition,
            startTimestamp,
            startDayTimestamp
        );

        expect(estimatedPosition.properties.assigningProcess).to.equal("time-closest from start - options: 1");
        expect(estimatedPosition.geometry.coordinates).to.deep.equal([14.47485, 50.1173]);
        expect(estimatedPosition.properties.time_delay).to.equal(72);
    });

    it("getEstimatedPoint should return proper delay (72s delayed, even this day was shift from CEST to CET)", async () => {
        // position on track
        const currentPosition = {
            geometry: {
                coordinates: [14.4747, 50.1173],
                type: "Point",
            },
            properties: {
                origin_time: "10:01:41",
                origin_timestamp: moment("2021-10-31T10:01:41+01:00").valueOf(),
            },
            type: "Feature",
        };
        const lastPosition = null;
        const startTimestamp = moment("2021-10-31T10:00:00+02:00").valueOf();

        // @ts-ignore
        sandbox
            .stub(worker["modelGTFSTrips"], "findByIdWithStopTimes")
            .callsFake(() => JSON.parse(fs.readFileSync(__dirname + "/./data/delay-stoptimes-01.json").toString()));
        // @ts-ignore
        sandbox
            .stub(worker["modelGTFSTrips"], "findByIdWithShapes")
            .callsFake(() => JSON.parse(fs.readFileSync(__dirname + "/./data/delay-shapes-01.json").toString()));
        // @ts-ignore
        const shapePoints = (await worker.getResultObjectForDelayCalculation("")).shape_points;

        // @ts-ignore
        const startDayTimestamp = await worker.getStartDayTimestamp(startTimestamp, shapePoints[0].time_scheduled_seconds);
        // @ts-ignore
        const estimatedPosition = await worker.getEstimatedPoint(
            shapePoints,
            currentPosition,
            lastPosition,
            startTimestamp,
            startDayTimestamp
        );

        expect(estimatedPosition.properties.assigningProcess).to.equal("time-closest from start - options: 1");
        expect(estimatedPosition.geometry.coordinates).to.deep.equal([14.47485, 50.1173]);
        expect(estimatedPosition.properties.time_delay).to.equal(72);
    });

    // eslint-disable-next-line max-len
    it("getEstimatedPoint should return proper delay (trip started day before, 72s delayed, even this day was shift from CEST to CET)", async () => {
        // position on track
        const currentPosition = {
            geometry: {
                coordinates: [14.4747, 50.1173],
                type: "Point",
            },
            properties: {
                origin_time: "04:01:41",
                origin_timestamp: moment("2021-10-31T04:01:41+01:00").valueOf(), //1615971610000
            },
            type: "Feature",
        };
        const lastPosition = null;
        const startTimestamp = moment("2021-10-31T04:00:00+01:00").valueOf();

        // @ts-ignore
        sandbox
            .stub(worker["modelGTFSTrips"], "findByIdWithStopTimes")
            .callsFake(() => JSON.parse(fs.readFileSync(__dirname + "/./data/delay-stoptimes-02.json").toString()));
        // @ts-ignore
        sandbox
            .stub(worker["modelGTFSTrips"], "findByIdWithShapes")
            .callsFake(() => JSON.parse(fs.readFileSync(__dirname + "/./data/delay-shapes-01.json").toString()));
        // @ts-ignore
        const shapePoints = (await worker.getResultObjectForDelayCalculation("")).shape_points;

        // @ts-ignore
        const startDayTimestamp = await worker.getStartDayTimestamp(startTimestamp, shapePoints[0].time_scheduled_seconds);

        // @ts-ignore
        const estimatedPosition = await worker.getEstimatedPoint(
            shapePoints,
            currentPosition,
            lastPosition,
            startTimestamp,
            startDayTimestamp
        );

        expect(estimatedPosition.properties.assigningProcess).to.equal("time-closest from start - options: 1");
        expect(estimatedPosition.geometry.coordinates).to.deep.equal([14.47485, 50.1173]);
        expect(estimatedPosition.properties.time_delay).to.equal(72);
    });

    // eslint-disable-next-line max-len
    it("getEstimatedPoint should return proper delay (trip started day before, 192s delayed, previous day was shift from CET to CEST)", async () => {
        // position on track
        const currentPosition = {
            geometry: {
                coordinates: [14.4747, 50.1173],
                type: "Point",
            },
            properties: {
                origin_time: "00:01:41",
                origin_timestamp: moment("2021-03-29T00:01:41+02:00").valueOf(), //1615971610000
            },
            type: "Feature",
        };
        const lastPosition = null;
        const startTimestamp = moment("2021-03-28T23:58:00+02:00").valueOf();

        // @ts-ignore
        sandbox
            .stub(worker["modelGTFSTrips"], "findByIdWithStopTimes")
            .callsFake(() => JSON.parse(fs.readFileSync(__dirname + "/./data/delay-stoptimes-03.json").toString()));
        // @ts-ignore
        sandbox
            .stub(worker["modelGTFSTrips"], "findByIdWithShapes")
            .callsFake(() => JSON.parse(fs.readFileSync(__dirname + "/./data/delay-shapes-01.json").toString()));
        // @ts-ignore
        const shapePoints = (await worker.getResultObjectForDelayCalculation("")).shape_points;

        // @ts-ignore
        const startDayTimestamp = await worker.getStartDayTimestamp(startTimestamp, shapePoints[0].time_scheduled_seconds);

        // @ts-ignore
        const estimatedPosition = await worker.getEstimatedPoint(
            shapePoints,
            currentPosition,
            lastPosition,
            startTimestamp,
            startDayTimestamp
        );

        expect(estimatedPosition.properties.assigningProcess).to.equal("time-closest from start - options: 1");
        expect(estimatedPosition.geometry.coordinates).to.deep.equal([14.47485, 50.1173]);
        expect(estimatedPosition.properties.time_delay).to.equal(192);
    });
});
