import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonFakeTimers, SinonSandbox, SinonStub } from "sinon";
import { config } from "@golemio/core/dist/integration-engine/config";
import { PostgresConnector, RedisConnector } from "@golemio/core/dist/integration-engine/connectors";
import { VehiclePositionsWorker } from "#ie/VehiclePositionsWorker";

chai.use(chaiAsPromised);

describe("VehiclePositionsWorker", () => {
    let worker: VehiclePositionsWorker;
    let sandbox: SinonSandbox;
    let sequelizeModelStub: Record<string, SinonStub>;
    let testData: Record<string, any>;
    let clock: SinonFakeTimers;

    beforeEach(() => {
        testData = {
            inserted: [
                {
                    cis_short_name: "999",
                    id: "999",
                    start_cis_stop_id: "999",
                    start_cis_stop_platform_code: "a",
                    start_timestamp: null,
                },
            ],
            updated: [],
        };

        sandbox = sinon.createSandbox();
        clock = sinon.useFakeTimers({
            now: new Date(2021, 2, 17, 10, 0),
            shouldAdvanceTime: true,
        });
        sequelizeModelStub = Object.assign({
            belongsTo: sandbox.stub(),
            hasMany: sandbox.stub(),
            hasOne: sandbox.stub(),
            removeAttribute: sandbox.stub(),
            upsert: sandbox.stub().resolves([{ id: 42 }, true]),
        });
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().callsFake(() => sequelizeModelStub),
                query: sandbox.stub().callsFake(() => [{ exists: true }]),
                transaction: sandbox.stub().callsFake(() => Object.assign({ commit: sandbox.stub() })),
            })
        );
        sandbox.stub(RedisConnector, "getConnection");

        worker = new VehiclePositionsWorker();
        sandbox
            .stub(worker["transformation"], "transform")
            .callsFake(() => Object.assign({ positions: [], stops: [], trips: [] }));
        sandbox.stub(worker["modelPositions"], "save");
        sandbox
            .stub(worker["modelPositions"], "getPositionsForUpdateDelay")
            // @ts-ignore
            .callsFake(() => [{ gtfs_trip_id: "0000", positions: [{ delay: null }] }]);
        // @ts-ignore
        sandbox.stub(worker, "computePositions").callsFake(() => [{ delay: 10, id: "12321" }]);
        sandbox.stub(worker["modelPositions"], "bulkUpdate");
        sandbox.stub(worker["modelStops"], "saveBySqlFunction");
        sandbox.stub(worker["modelTrips"], "bulkUpsert").callsFake(() => Promise.resolve(testData as any));
        sandbox.stub(worker["modelTrips"], "findAll").callsFake((_) => Object.assign([]));
        sandbox.stub(worker["modelTrips"], "findGTFSTripId").callsFake(() =>
            Object.assign([
                { id: "2021-02-03T10:23:00Z_none_S45_1573", block_id: 1 },
                { id: "2021-02-03T10:23:00Z_none_S45_1573_gtfs_trip_id_1345_1573_201213", block_id: 1 },
            ])
        );

        sandbox.stub(worker["modelTrips"], "update");
        // @ts-ignore
        sandbox.stub(worker, "sendMessageToExchange");
        sandbox.stub(worker["delayComputationTripsModel"], "getData").callsFake(() => Object.assign({ shape_points: [] }));
        // @ts-ignore
        sandbox.stub(worker, "getEstimatedPoint").callsFake(() =>
            Object.assign({
                properties: { time_delay: 0, shape_dist_traveled: 0, next_stop_id: "00" },
            })
        );
        sandbox.stub(worker["gtfsRtModel"], "save");
    });

    afterEach(() => {
        sandbox.restore();
        clock.restore();
    });

    it("should calls the correct methods by saveDataToDB method", async () => {
        await worker.saveDataToDB({ content: Buffer.from(JSON.stringify({ m: { spoj: {} } })) });
        sandbox.assert.calledOnce(worker["transformation"].transform as SinonStub);
        sandbox.assert.calledOnce(worker["modelTrips"].bulkUpsert as SinonStub);
        sandbox.assert.calledWith(worker["modelTrips"].bulkUpsert as SinonStub, []);
        sandbox.assert.calledOnce(worker["sendMessageToExchange"] as SinonStub);
        sandbox.assert.callOrder(
            worker["transformation"].transform as SinonStub,
            worker["modelTrips"].bulkUpsert as SinonStub,
            worker["sendMessageToExchange"] as SinonStub
        );
        sandbox.assert.callCount(PostgresConnector.getConnection as SinonStub, 7);
    });

    it("should calls the correct methods by saveStopsToDB method", async () => {
        await worker.saveStopsToDB({ content: Buffer.from(JSON.stringify({ m: { spoj: {} } })) });
        sandbox.assert.calledOnce(worker["transformation"].transform as SinonStub);
        sandbox.assert.calledOnce(worker["modelStops"].saveBySqlFunction as SinonStub);
        sandbox.assert.calledWith(worker["modelStops"].saveBySqlFunction as SinonStub, []);
        sandbox.assert.callOrder(
            worker["transformation"].transform as SinonStub,
            worker["modelStops"].saveBySqlFunction as SinonStub
        );
        sandbox.assert.callCount(PostgresConnector.getConnection as SinonStub, 7);
    });

    it("should calls the correct methods by generateGtfsRt method", async () => {
        await worker.generateGtfsRt({ content: Buffer.from("0") });
        sandbox.assert.calledOnce(worker["modelTrips"].findAll as SinonStub);
        sandbox.assert.callCount(worker["gtfsRtModel"].save as SinonStub, 4);
    });

    it("should calls the correct methods by updateGTFSTripId method", async () => {
        await worker.updateGTFSTripId({
            content: Buffer.from(
                JSON.stringify({
                    data: [
                        {
                            cis_line_short_name: "S45",
                            id: "2021-02-03T10:23:00Z_none_S45_1573",
                            start_asw_stop_id: null,
                            start_cis_stop_id: 5454396,
                            start_timestamp: 1612347780000,
                            // tslint:disable-next-line: object-literal-sort-keys
                            agency_name_real: null,
                            agency_name_scheduled: "ČESKÉ DRÁHY",
                            cis_line_id: "none",
                            cis_trip_number: 1573,
                            origin_route_name: null,
                            sequence_id: null,
                            vehicle_registration_number: null,
                            vehicle_type_id: 0,
                            wheelchair_accessible: true,
                        },
                    ],
                    positions: [
                        {
                            asw_last_stop_id: null,
                            bearing: null,
                            cis_last_stop_id: 5457066,
                            cis_last_stop_sequence: 15,
                            delay_stop_arrival: 0,
                            delay_stop_departure: 0,
                            is_canceled: false,
                            lat: 50.238575,
                            lng: 14.3130083,
                            origin_time: "11:23:00",
                            origin_timestamp: 1612347780000,
                            speed: null,
                            tracking: 1,
                            trips_id: "2021-02-03T10:23:00Z_none_S45_1573",
                        },
                        {
                            asw_last_stop_id: null,
                            bearing: null,
                            cis_last_stop_id: 5457066,
                            cis_last_stop_sequence: 15,
                            delay_stop_arrival: 0,
                            delay_stop_departure: 0,
                            is_canceled: false,
                            lat: 50.238575,
                            lng: 14.3130083,
                            origin_time: "11:23:00",
                            origin_timestamp: 1612347780000,
                            speed: null,
                            tracking: 1,
                            trips_id: "2021-02-03T10:23:00Z_none_S45_1573_gtfs_trip_id_1345_1573_201213",
                        },
                    ],
                })
            ),
        });

        sandbox.assert.calledOnce(worker["modelTrips"].findGTFSTripId as SinonStub);
        sandbox.assert.calledOnce(worker["modelPositions"].save as SinonStub);
        sandbox.assert.calledWith(worker["modelPositions"].save as SinonStub, [
            {
                asw_last_stop_id: null,
                bearing: null,
                cis_last_stop_id: 5457066,
                cis_last_stop_sequence: 15,
                delay_stop_arrival: 0,
                delay_stop_departure: 0,
                is_canceled: false,
                lat: 50.238575,
                lng: 14.3130083,
                origin_time: "11:23:00",
                origin_timestamp: 1612347780000,
                speed: null,
                tracking: 1,
                trips_id: "2021-02-03T10:23:00Z_none_S45_1573",
            },
            {
                asw_last_stop_id: null,
                bearing: null,
                cis_last_stop_id: 5457066,
                cis_last_stop_sequence: 15,
                delay_stop_arrival: 0,
                delay_stop_departure: 0,
                is_canceled: false,
                lat: 50.238575,
                lng: 14.3130083,
                origin_time: "11:23:00",
                origin_timestamp: 1612347780000,
                speed: null,
                tracking: 1,
                trips_id: "2021-02-03T10:23:00Z_none_S45_1573_gtfs_trip_id_1345_1573_201213",
            },
        ]);
        sandbox.assert.calledOnce(worker["sendMessageToExchange"] as SinonStub);
    });

    it("should calls the correct methods by updateDelay method", async () => {
        await worker.updateDelay({
            content: Buffer.from(
                JSON.stringify({
                    positions: [],
                    updatedTrips: new Array(0),
                })
            ),
        });

        sandbox.assert.calledOnce(worker["modelPositions"].getPositionsForUpdateDelay as SinonStub);
        sandbox.assert.calledOnce(worker["delayComputationTripsModel"].getData as SinonStub);
        sandbox.assert.calledOnce(worker["computePositions"] as SinonStub);
    });

    it("getStopTimesForDelayComputation should return correct data", async () => {
        const step = 0.17;
        sandbox.stub(config.vehiclePositions, "stepBetweenPoints").value(step);

        const shapes = [
            {
                shape_pt_lat: 50.103323,
                shape_pt_lon: 14.455949,
            },
            {
                shape_pt_lat: 50.103358,
                shape_pt_lon: 14.462077,
            },
        ];

        const points: Array<Record<string, any>> = await worker["getShapesAnchorPointsForDelayComputation"]("test", shapes);

        points.forEach((point, i) => {
            expect(point.coordinates?.[0]).to.be.at.least(Math.round(shapes[0].shape_pt_lon * 100000) / 100000);
            expect(point.coordinates?.[1]).to.be.at.least(Math.round(shapes[0].shape_pt_lat * 100000) / 100000);
            expect(point.shape_dist_traveled).to.eq(i * step);
        });
    });
});
