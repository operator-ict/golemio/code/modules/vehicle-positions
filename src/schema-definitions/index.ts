import { SchemaDefinition } from "mongoose";
import Sequelize from "@golemio/core/dist/shared/sequelize";

// MSO = Mongoose SchemaObject
// SDMA = Sequelize DefineModelAttributes

/* tslint:disable object-literal-sort-keys object-literal-key-quotes trailing-comma */
const datasourceJsonSchema = {
    // reusable definitions
    definitions: {
        spojObj: {
            type: "object",
            properties: {
                $: {
                    title: "$",
                    type: "object",
                    properties: {
                        alias: { type: "string" },
                        azimut: { type: "string" },
                        cpoz: { type: "string" },
                        dopr: { type: "string" },
                        doprSkut: { type: "string" },
                        info: { type: "string" },
                        kmenl: { type: "string" },
                        lat: { type: "string" },
                        lin: { type: "string" },
                        lng: { type: "string" },
                        np: { type: "string" },
                        po: { type: "string" },
                        rychl: { type: "string" },
                        sled: { type: "string" },
                        spoj: { type: "string" },
                        t: { type: "string" },
                        vuzevc: { type: "string" },
                        zast: { type: "string" },
                        zpoz_prij: { type: "string" },
                        zrus: { type: "string" },
                    },
                    required: [],
                },
                zast: {
                    type: "array",
                    items: {
                        title: "itemOf_zast",
                        type: "object",
                        properties: {
                            $: {
                                title: "$",
                                type: "object",
                                properties: {
                                    odj: { type: "string" },
                                    prij: { type: "string" },
                                    stan: { type: "string" },
                                    zast: { type: "string" },
                                    asw: { type: "string" },
                                    zpoz_odj: { type: "string" },
                                    zpoz_prij: { type: "string" },
                                    zpoz_typ: { type: "string" },
                                    zpoz_typ_prij: { type: "string" },
                                },
                                required: [],
                            },
                        },
                    },
                },
            },
        },
    },

    // main schema
    type: "object",
    properties: {
        m: {
            title: "m",
            type: "object",
            properties: {
                spoj: {
                    oneOf: [
                        // property `spoj` can be object or array of objects
                        {
                            type: "array",
                            items: { $ref: "#/definitions/spojObj" }, // reference to definition
                        },
                        {
                            $ref: "#/definitions/spojObj", // reference to definition
                        },
                    ],
                },
            },
        },
    },
};
/* tslint:enable */

// INSERT or UPDATE
const outputTripsSDMA: Sequelize.ModelAttributes<any> = {
    agency_name_real: Sequelize.STRING, // doprSkut "Jaroslav Štěpánek"
    agency_name_scheduled: Sequelize.STRING, // dopr "Jaroslav Štěpánek"
    cis_line_id: Sequelize.STRING, // lin "100155", "X7"
    cis_line_short_name: Sequelize.STRING, // alias "155", "X7"
    cis_trip_number: Sequelize.INTEGER, // spoj "40"
    gtfs_block_id: Sequelize.STRING, // 1345_1573_201213
    gtfs_route_id: Sequelize.STRING,
    gtfs_route_short_name: Sequelize.STRING,
    gtfs_route_type: Sequelize.INTEGER,
    gtfs_trip_headsign: Sequelize.STRING,
    gtfs_trip_id: Sequelize.STRING,
    // ⬐ hash(start_timestamp, cis_id, cis_short_name, cis_number);
    id: {
        primaryKey: true,
        type: Sequelize.STRING,
    },
    last_position_id: Sequelize.INTEGER,
    origin_route_name: Sequelize.STRING, // kmenl "110"
    sequence_id: Sequelize.INTEGER, // po "1"
    start_asw_stop_id: Sequelize.STRING, // zast[0].zast "1234/1"
    start_cis_stop_id: Sequelize.INTEGER, // zast[0].zast "57517"
    start_cis_stop_platform_code: Sequelize.STRING, // zast[0].stan "C"
    start_time: Sequelize.TIME, // zast[0].prij "" || "11:00"
    // ⬐ Převod příjezdu první zastávky na date (pozor, čas 23:59 který přišel v 0:01 bude mít včerejší datum!)
    start_timestamp: Sequelize.BIGINT, // zast[0].prij "" || "11:00"
    vehicle_registration_number: Sequelize.INTEGER, // vuzevc "1030"
    vehicle_type_id: Sequelize.INTEGER, // t "3"
    wheelchair_accessible: Sequelize.BOOLEAN, // np "true"

    // ⬐ Auditni pole
    create_batch_id: { type: Sequelize.BIGINT }, // ID vstupní dávky
    created_at: { type: Sequelize.DATE }, // Čas vložení
    created_by: { type: Sequelize.STRING(150) }, // identikace uživatele/procesu, který záznam vložil
    update_batch_id: { type: Sequelize.BIGINT }, // ID poslední dávky, která záznam modifikovala
    updated_at: { type: Sequelize.DATE }, // Čas poslední modifikace
    updated_by: { type: Sequelize.STRING(150) }, // Identikace uživatele/procesu, který poslední měnil záznam
};

// only for Validator
const outputTripsMSO: SchemaDefinition = {
    agency_name_real: { type: String },
    agency_name_scheduled: { type: String },
    cis_line_id: { type: String },
    cis_line_short_name: { type: String },
    cis_trip_number: { type: Number },
    gtfs_block_id: { type: String },
    gtfs_route_id: { type: String },
    gtfs_route_short_name: { type: String },
    gtfs_route_type: { type: Number },
    gtfs_trip_headsign: { type: String },
    gtfs_trip_id: { type: String },
    origin_route_name: { type: String },
    sequence_id: { type: Number },
    vehicle_registration_number: { type: String },
    vehicle_type_id: { type: Number, required: true },
    wheelchair_accessible: { type: Boolean },
};

// only INSERT
const outputVehicleTypesSDMA: Sequelize.ModelAttributes<any> = {
    description_cs: Sequelize.STRING,
    description_en: Sequelize.STRING,
    id: {
        primaryKey: true,
        type: Sequelize.BIGINT,
    },
};

// for validation
const outputVehicleTypesMSO: SchemaDefinition = {
    description_cs: { type: String, required: true },
    description_en: { type: String, required: true },
    id: { type: Number, required: true },
};

// only INSERT
const outputPositionsSDMA: Sequelize.ModelAttributes<any> = {
    asw_last_stop_id: Sequelize.STRING, // zast "28010"
    bearing: Sequelize.INTEGER, // azimut "0"
    cis_last_stop_id: Sequelize.INTEGER, // zast "28010"
    cis_last_stop_sequence: Sequelize.INTEGER, // stop_sequence
    delay: Sequelize.INTEGER,
    delay_stop_arrival: Sequelize.INTEGER, // zpoz_prij "426"
    delay_stop_departure: Sequelize.INTEGER, // zpoz_odj "426"
    id: {
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT,
    },
    is_canceled: Sequelize.BOOLEAN, // zrus "false"
    last_stop_arrival_time: Sequelize.BIGINT,
    last_stop_departure_time: Sequelize.BIGINT,
    last_stop_id: Sequelize.STRING,
    last_stop_sequence: Sequelize.INTEGER,
    lat: Sequelize.DECIMAL, // lat "50.08323"
    lng: Sequelize.DECIMAL, // lng "14.51035"
    next_stop_arrival_time: Sequelize.BIGINT,
    next_stop_departure_time: Sequelize.BIGINT,
    next_stop_id: Sequelize.STRING,
    next_stop_sequence: Sequelize.INTEGER,
    origin_time: Sequelize.TIME, // cpoz "11:09:06"
    origin_timestamp: Sequelize.BIGINT, // cpoz "11:09:06"
    shape_dist_traveled: Sequelize.DECIMAL,
    speed: Sequelize.INTEGER, // rychl "0"
    state_position: Sequelize.STRING,
    state_process: Sequelize.STRING,
    this_stop_id: Sequelize.STRING,
    tracking: Sequelize.INTEGER, // sled "2"
    // ⬐ hash(start_timestamp, cis_id, cis_short_name, cis_number);
    trips_id: { type: Sequelize.STRING },

    // ⬐ Auditni pole
    create_batch_id: { type: Sequelize.BIGINT }, // ID vstupní dávky
    created_at: { type: Sequelize.DATE }, // Čas vložení
    created_by: { type: Sequelize.STRING(150) }, // identikace uživatele/procesu, který záznam vložil
    update_batch_id: { type: Sequelize.BIGINT }, // ID poslední dávky, která záznam modifikovala
    updated_at: { type: Sequelize.DATE }, // Čas poslední modifikace
    updated_by: { type: Sequelize.STRING(150) }, // Identikace uživatele/procesu, který poslední měnil záznam
};

// only for Validator
const outputPositionsMSO: SchemaDefinition = {
    bearing: { type: Number },
    delay: { type: Number },
    delay_stop_arrival: { type: Number },
    delay_stop_departure: { type: Number },
    is_canceled: { type: Boolean },
    last_stop_arrival_time: { type: String },
    last_stop_departure_time: { type: String },
    last_stop_id: { type: String },
    last_stop_sequence: { type: Number },
    lat: { type: Number, required: false },
    lng: { type: Number, required: false },
    next_stop_arrival_time: { type: String },
    next_stop_departure_time: { type: String },
    next_stop_id: { type: String },
    next_stop_sequence: { type: Number },
    origin_timestamp: { type: Number },
    shape_dist_traveled: { type: Number },
    speed: { type: Number },
    state_position: { type: String },
    state_process: { type: String },
    this_stop_id: { type: String, required: false },
};

// INSERT or UPDATE
const outputStopsSDMA: Sequelize.ModelAttributes<any> = {
    arrival_delay_type: Sequelize.INTEGER, // zpoz_typ_prij "0"
    arrival_time: Sequelize.TIME, // prij "" || "11:00"
    // ⬐ Převod na timestamp (pozor, čas 23:59 který přišel v 0:01 bude mít včerejší datum!)
    arrival_timestamp: Sequelize.BIGINT, // prij "" || "11:00"
    asw_stop_id: Sequelize.STRING, // zast v ASW např. "628/4"
    cis_stop_id: Sequelize.INTEGER, // zast "57517"
    cis_stop_platform_code: Sequelize.STRING, // stan "C"
    // ⬐ Doplnit podle poradi elementu v xml, cislovano od 1
    cis_stop_sequence: {
        primaryKey: true,
        type: Sequelize.INTEGER,
    },
    delay_arrival: Sequelize.INTEGER, // zpoz_prij "311"
    delay_departure: Sequelize.INTEGER, // zpoz_odj "311"
    delay_type: Sequelize.INTEGER, // zpoz_typ "3"
    departure_time: Sequelize.TIME, // odj "" || "11:00"
    // ⬐ Převod na timestamp (pozor, čas 23:59 který přišel v 0:01 bude mít včerejší datum!)
    departure_timestamp: Sequelize.BIGINT, // odj "" || "11:00"
    // ⬐ hash(start_timestamp, cis_id, cis_short_name, cis_number);
    trips_id: {
        primaryKey: true,
        type: Sequelize.STRING,
    },

    // ⬐ Auditni pole
    create_batch_id: { type: Sequelize.BIGINT }, // ID vstupní dávky
    created_at: { type: Sequelize.DATE }, // Čas vložení
    created_by: { type: Sequelize.STRING(150) }, // identikace uživatele/procesu, který záznam vložil
    update_batch_id: { type: Sequelize.BIGINT }, // ID poslední dávky, která záznam modifikovala
    updated_at: { type: Sequelize.DATE }, // Čas poslední modifikace
    updated_by: { type: Sequelize.STRING(150) }, // Identikace uživatele/procesu, který poslední měnil záznam
};

// only for Validator
const outputStopsMSO: SchemaDefinition = {
    arrival_delay_type: { type: Number },
    arrival_time: { type: String },
    arrival_timestamp: { type: Number },
    asw_stop_id: { type: String },
    cis_stop_id: { type: Number },
    cis_stop_platform_code: { type: String },
    cis_stop_sequence: { type: Number, required: true },
    delay_arrival: { type: Number },
    delay_departure: { type: Number },
    delay_type: { type: Number },
    departure_time: { type: String },
    departure_timestamp: { type: Number },
    trips_id: { type: String, required: true },
};

// V1 Schemas

// INSERT or UPDATE
const outputV1TripsSDMA: Sequelize.ModelAttributes<any> = {
    cis_agency_name: Sequelize.STRING, // dopr "Jaroslav Štěpánek"
    cis_id: Sequelize.STRING, // lin "100155"
    cis_number: Sequelize.INTEGER, // spoj "40"
    cis_order: Sequelize.INTEGER, // po "1"
    cis_parent_route_name: Sequelize.STRING, // kmenl "110"
    cis_real_agency_name: Sequelize.STRING, // doprSkut "Jaroslav Štěpánek"
    cis_short_name: Sequelize.STRING, // alias "155"
    cis_vehicle_registration_number: Sequelize.INTEGER, // vuzevc "1030"
    gtfs_route_id: Sequelize.STRING,
    gtfs_route_short_name: Sequelize.STRING,
    gtfs_trip_id: Sequelize.STRING,
    // ⬐ hash(start_timestamp, cis_id, cis_short_name, cis_number);
    id: {
        primaryKey: true,
        type: Sequelize.STRING,
    },
    start_cis_stop_id: Sequelize.INTEGER, // zast[0].zast "57517"
    start_cis_stop_platform_code: Sequelize.STRING, // zast[0].stan "C"
    start_time: Sequelize.TIME, // zast[0].prij "" || "11:00"
    // ⬐ Převod příjezdu první zastávky na date (pozor, čas 23:59 který přišel v 0:01 bude mít včerejší datum!)
    start_timestamp: Sequelize.BIGINT, // zast[0].prij "" || "11:00"
    vehicle_type: Sequelize.INTEGER, // t "3"
    wheelchair_accessible: Sequelize.BOOLEAN, // np "true"

    // ⬐ Auditni pole
    create_batch_id: { type: Sequelize.BIGINT }, // ID vstupní dávky
    created_at: { type: Sequelize.DATE }, // Čas vložení
    created_by: { type: Sequelize.STRING(150) }, // identikace uživatele/procesu, který záznam vložil
    update_batch_id: { type: Sequelize.BIGINT }, // ID poslední dávky, která záznam modifikovala
    updated_at: { type: Sequelize.DATE }, // Čas poslední modifikace
    updated_by: { type: Sequelize.STRING(150) }, // Identikace uživatele/procesu, který poslední měnil záznam
};

// only for Validator
const outputV1TripsMSO: SchemaDefinition = {
    cis_agency_name: { type: String },
    cis_id: { type: String, required: true },
    cis_number: { type: Number, required: true },
    cis_order: { type: Number },
    cis_parent_route_name: { type: Number },
    cis_real_agency_name: { type: String },
    cis_short_name: { type: String, required: true },
    cis_vehicle_registration_number: { type: Number },
    gtfs_route_id: { type: String },
    gtfs_route_short_name: { type: String },
    gtfs_trip_id: { type: String },
    id: { type: String, required: true },
    start_cis_stop_id: { type: Number },
    start_cis_stop_platform_code: { type: String },
    start_time: { type: String, required: true },
    start_timestamp: { type: Number, required: true },
    vehicle_type: { type: Number },
    wheelchair_accessible: { type: Boolean },
};

// only INSERT
const outputV1PositionsSDMA: Sequelize.ModelAttributes<any> = {
    bearing: Sequelize.INTEGER, // azimut "0"
    cis_last_stop_id: Sequelize.INTEGER, // zast "28010"
    cis_last_stop_sequence: Sequelize.INTEGER, // stop_sequence
    delay: Sequelize.INTEGER,
    delay_stop_arrival: Sequelize.INTEGER, // zpoz_prij "426"
    delay_stop_departure: Sequelize.INTEGER, // zpoz_odj "426"
    gtfs_last_stop_id: Sequelize.STRING,
    gtfs_last_stop_sequence: Sequelize.INTEGER,
    gtfs_next_stop_id: Sequelize.STRING,
    gtfs_next_stop_sequence: Sequelize.INTEGER,
    gtfs_shape_dist_traveled: Sequelize.DECIMAL,
    id: {
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT,
    },
    is_canceled: Sequelize.BOOLEAN, // zrus "false"
    lat: Sequelize.DECIMAL, // lat "50.08323"
    lng: Sequelize.DECIMAL, // lng "14.51035"
    origin_time: Sequelize.TIME, // cpoz "11:09:06"
    // ⬐ Převod na timestamp (pozor, čas 23:59 který přišel v 0:01 bude mít včerejší datum!)
    origin_timestamp: Sequelize.BIGINT, // cpoz "11:09:06"
    speed: Sequelize.INTEGER, // rychl "0"
    tracking: Sequelize.INTEGER, // sled "2"
    // ⬐ hash(start_timestamp, cis_id, cis_short_name, cis_number);
    trips_id: { type: Sequelize.STRING },

    // ⬐ Auditni pole
    create_batch_id: { type: Sequelize.BIGINT }, // ID vstupní dávky
    created_at: { type: Sequelize.DATE }, // Čas vložení
    created_by: { type: Sequelize.STRING(150) }, // identikace uživatele/procesu, který záznam vložil
    update_batch_id: { type: Sequelize.BIGINT }, // ID poslední dávky, která záznam modifikovala
    updated_at: { type: Sequelize.DATE }, // Čas poslední modifikace
    updated_by: { type: Sequelize.STRING(150) }, // Identikace uživatele/procesu, který poslední měnil záznam
};

// only for Validator
const outputV1PositionsMSO: SchemaDefinition = {
    bearing: { type: Number },
    cis_last_stop_id: { type: Number },
    cis_last_stop_sequence: { type: Number },
    delay: { type: Number },
    delay_stop_arrival: { type: Number },
    delay_stop_departure: { type: Number },
    gtfs_last_stop_id: { type: String },
    gtfs_last_stop_sequence: { type: Number },
    gtfs_next_stop_id: { type: String },
    gtfs_next_stop_sequence: { type: Number },
    gtfs_shape_dist_traveled: { type: Number },
    is_canceled: { type: Boolean },
    lat: { type: Number },
    lng: { type: Number },
    origin_time: { type: String },
    origin_timestamp: { type: Number },
    speed: { type: Number },
    tracking: { type: Number },
    trips_id: { type: String, required: true },
};

const forExport = {
    datasourceJsonSchema,
    lastPositions: {
        name: "VehiclePositionsLastPositionsView",
        outputMongooseSchemaObject: outputPositionsMSO,
        outputSequelizeAttributes: outputPositionsSDMA,
        pgTableName: "v_vehiclepositions_last_position",
    },
    name: "VehiclePositions",
    positions: {
        name: "VehiclePositionsPositions",
        outputMongooseSchemaObject: outputPositionsMSO,
        outputSequelizeAttributes: outputPositionsSDMA,
        pgTableName: "vehiclepositions_positions",
    },
    stops: {
        name: "VehiclePositionsStops",
        outputMongooseSchemaObject: outputStopsMSO,
        outputSequelizeAttributes: outputStopsSDMA,
        pgTableName: "vehiclepositions_stops",
    },
    trips: {
        name: "VehiclePositionsTrips",
        outputMongooseSchemaObject: outputTripsMSO,
        outputSequelizeAttributes: outputTripsSDMA,
        pgTableName: "vehiclepositions_trips",
    },
    vehicleTypes: {
        name: "VehiclePositionsVehicleTypes",
        outputMongooseSchemaObject: outputVehicleTypesMSO,
        outputSequelizeAttributes: outputVehicleTypesSDMA,
        pgTableName: "vehiclepositions_vehicle_types",
    },

    v1: {
        lastPositions: {
            name: "VehiclePositionsLastPositionsView",
            outputMongooseSchemaObject: outputV1PositionsMSO,
            outputSequelizeAttributes: outputV1PositionsSDMA,
            pgTableName: "v_vehiclepositions_last_position_v1",
        },
        positions: {
            name: "VehiclePositionsPositions",
            outputMongooseSchemaObject: outputV1PositionsMSO,
            outputSequelizeAttributes: outputV1PositionsSDMA,
            pgTableName: "v_vehiclepositions_positions_v1",
        },
        trips: {
            name: "VehiclePositionsTrips",
            outputMongooseSchemaObject: outputV1TripsMSO,
            outputSequelizeAttributes: outputV1TripsSDMA,
            pgTableName: "v_vehiclepositions_trips_v1",
        },
    },
};

export { forExport as VehiclePositions };
