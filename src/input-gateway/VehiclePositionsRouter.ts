// Import only what we need from express
import { NextFunction, Request, Response, Router } from "@golemio/core/dist/shared/express";
import {
    checkContentTypeMiddleware,
    INumberOfRecordsEventData,
    loggerEvents,
    LoggerEventType,
} from "@golemio/core/dist/input-gateway/helpers";
import { VehiclePositionsController } from "./";

export class VehiclePositionsRouter {
    // Assign router to the express.Router() instance
    public router: Router = Router();
    private controller: VehiclePositionsController = new VehiclePositionsController();

    constructor() {
        this.initRoutes();
    }

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     */
    private initRoutes = (): void => {
        this.router.post("/", checkContentTypeMiddleware(["text/xml"]), this.Post);
    };

    private Post = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            await this.controller.processData(req.body);
            // logging number of records
            const dataToEmit: INumberOfRecordsEventData = {
                numberOfRecords:
                    req.body && req.body.m && req.body.m.spoj && req.body.m.spoj instanceof Array
                        ? req.body.m.spoj.length
                        : req.body && req.body.m && req.body.m.spoj
                        ? 1
                        : 0,
                req,
            };
            loggerEvents.emit(LoggerEventType.NumberOfRecords, dataToEmit);
            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };
}

const vehiclePositionsRouter = new VehiclePositionsRouter().router;

export { vehiclePositionsRouter };
