import moment from "@golemio/core/dist/shared/moment-timezone";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { VehiclePositions } from "#sch/index";

export class VehiclePositionsTransformation extends BaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = VehiclePositions.name;
    }

    /**
     * Overrides BaseTransformation::transform
     */
    public transform = async (data: any | any[]): Promise<any | any[]> => {
        const res: Record<string, any[]> = {
            positions: [],
            stops: [],
            trips: [],
        };

        if (Array.isArray(data)) {
            for (const element of data) {
                const elemRes = await this.transformElement(element);
                if (elemRes) {
                    res.positions.push(elemRes.position);
                    res.stops = res.stops.concat(elemRes.stops);
                    res.trips.push(elemRes.trip);
                }
            }
        } else {
            const elemRes = await this.transformElement(data);
            if (elemRes) {
                res.positions.push(elemRes.position);
                res.stops = res.stops.concat(elemRes.stops);
                res.trips.push(elemRes.trip);
            }
        }
        return res;
    };

    /**
     * Deduce UTC timestamp of start of trip and UTC timestamp of current position
     *
     * @param {any} firstStopXML - parsed object of first stop
     * @param {string} positionTimestampXML - XML attribute of local timestamp of position (HH:mm:ss)
     * @returns {{ startTimestamp: moment.Moment; positionTimestamp: moment.Moment }}
     */
    protected deduceTimestamps = async (
        firstStopXML: any,
        positionTimestampXML: string
    ): Promise<{ startTimestamp: moment.Moment; positionTimestamp: moment.Moment }> => {
        const now = moment.tz("Europe/Prague");
        let isOverMidnight = 0;

        // creating startDate and timestamp from zast[0].prij and cpoz
        const startTimestamp = now.clone();
        let startDatePlain = firstStopXML.$.prij || firstStopXML.$.odj;
        startDatePlain = startDatePlain.split(":");
        // eslint-disable-next-line prettier/prettier
        startTimestamp.hour(parseInt(startDatePlain[0], 10)).minute(parseInt(startDatePlain[1], 10)).second(0).millisecond(0);

        // midnight checking
        isOverMidnight = this.checkMidnight(now, startTimestamp); // returns -1, 1 or 0
        startTimestamp.add(isOverMidnight, "d");

        const positionTimestamp = now.clone();
        const timestampPlain = positionTimestampXML.split(":");
        positionTimestamp
            .hour(parseInt(timestampPlain[0], 10))
            .minute(parseInt(timestampPlain[1], 10))
            .second(parseInt(timestampPlain[2], 10))
            .millisecond(0);

        // midnight checking
        isOverMidnight = this.checkMidnight(now, positionTimestamp); // returns -1, 1 or 0
        positionTimestamp.add(isOverMidnight, "d");

        return {
            startTimestamp,
            positionTimestamp,
        };
    };

    protected transformElement = async (element: any): Promise<{ position: any; stops: any[]; trip: any }> => {
        const attributes = element.$;
        const stops = element.zast;

        if (!attributes.cpoz) {
            return null as any;
        }

        const { startTimestamp, positionTimestamp } = await this.deduceTimestamps(stops[0], attributes.cpoz);

        // set "none" if lin or alias is null, undefined or "" (empty string)
        attributes.lin = attributes.lin === "" ? "none" : attributes.lin ?? "none";
        attributes.alias = attributes.alias === "" ? "none" : attributes.alias ?? "none";

        // primary key -> start_timestamp, cis_id, cis_short_name, cis_number
        const primaryKey =
            `${startTimestamp.utc().format()}` + `_${attributes.lin}` + `_${attributes.alias}` + `_${attributes.spoj}`;

        /*
            MPV route types:

            Metro:                           1
            Tramvaj:                         2
            Bus městský:                     3
            Bus regionální:                  4
            Bus noční:                       5
            Noční tramvaj:                   6
            Náhradní autobusová doprava:     7
            Lanovka:                         8
            Školní:                          9
            Invalidní:                      10
            Smluvní:                        11
            Loď:                            12
            Vlak:                           13
            Náhradní doprava za vlak:       14
            Náhradní tramvajová doprava:    15
            Bus noční regionální:           16
            Ostatní:                        17
            Trolejbus:                      18
        */

        // exceptions for DP PRAHA agency trips
        const agencyDPPRAHAException = attributes.dopr === "DP PRAHA";

        // those trips of DP PRAHA agency trams excluded (normal - 2, night - 6, exceptional - 15)
        const tripsTramsExcludedDPPRAHAException =
            attributes.dopr === "DP PRAHA" && attributes.t !== "2" && attributes.t !== "6" && attributes.t !== "15";

        const vehicleRegistrationNumber = tripsTramsExcludedDPPRAHAException ? null : attributes.vuzevc;
        const bearing = agencyDPPRAHAException ? null : attributes.azimut;
        const speed = agencyDPPRAHAException ? null : attributes.rychl;
        const aswLastStopId = agencyDPPRAHAException ? attributes.asw : null;
        const cisLastStopId = agencyDPPRAHAException ? null : attributes.zast;

        const res: Record<string, any> = {
            position: {
                asw_last_stop_id: aswLastStopId ? this.formatASWStopId(aswLastStopId) : null,
                bearing: bearing ? this.fixSourceNegativeBearing(parseInt(bearing, 10)) : null,
                cis_last_stop_id: cisLastStopId ? parseInt(cisLastStopId, 10) : null,
                cis_last_stop_sequence: null,
                delay_stop_arrival: attributes.zpoz_prij ? parseInt(attributes.zpoz_prij, 10) : null,
                delay_stop_departure: attributes.zpoz_odj ? parseInt(attributes.zpoz_odj, 10) : null,
                is_canceled: attributes.zrus === "true" ? true : false,
                lat: attributes.lat ? parseFloat(attributes.lat) : null,
                lng: attributes.lng ? parseFloat(attributes.lng) : null,
                origin_time: attributes.cpoz,
                origin_timestamp: positionTimestamp.utc().valueOf(),
                speed: speed ? parseInt(speed, 10) : null,
                state_position: "unknown",
                state_process: "input",
                tracking: attributes.sled ? parseInt(attributes.sled, 10) : null,
                trips_id: primaryKey,
            },
            stops: [],
            trip: {
                agency_name_real: attributes.doprSkut ? attributes.doprSkut : null,
                agency_name_scheduled: attributes.dopr ? attributes.dopr : null,
                cis_line_id: attributes.lin,
                cis_line_short_name: attributes.alias,
                cis_trip_number: parseInt(attributes.spoj, 10),
                id: primaryKey,
                origin_route_name: attributes.kmenl ? parseInt(attributes.kmenl, 10) : null,
                sequence_id: attributes.po !== undefined ? parseInt(attributes.po, 10) : null,
                start_asw_stop_id: agencyDPPRAHAException ? this.formatASWStopId(stops[0].$.asw) : null,
                start_cis_stop_id: !agencyDPPRAHAException ? parseInt(stops[0].$.zast, 10) : null,
                start_cis_stop_platform_code: stops[0].$.stan,
                start_time: stops[0].$.prij !== "" ? stops[0].$.prij : stops[0].$.odj,
                start_timestamp: startTimestamp.utc().valueOf(),
                vehicle_registration_number: vehicleRegistrationNumber ? parseInt(vehicleRegistrationNumber, 10) : null,
                vehicle_type_id: attributes.t ? parseInt(attributes.t, 10) : null,
                wheelchair_accessible: attributes.np === "true" ? true : false,
            },
        };

        stops.forEach((stop: any, i: number) => {
            let arrival: moment.Moment;
            let departure: moment.Moment;
            let isOverMidnight = 0;
            const now = moment.tz("Europe/Prague");

            // creating arival from stop.$.prij
            if (stop.$.prij && stop.$.prij !== "") {
                arrival = moment.tz("Europe/Prague");
                const arrivalPlain = stop.$.prij.split(":");
                arrival.hour(parseInt(arrivalPlain[0], 10));
                arrival.minute(parseInt(arrivalPlain[1], 10));
                arrival.second(0);
                arrival.millisecond(0);

                // midnight checking
                isOverMidnight = this.checkMidnight(now, arrival); // returns -1, 1 or 0
                arrival.add(isOverMidnight, "d");
            }
            // creating departure from stop.$.odj
            if (stop.$.odj && stop.$.odj !== "") {
                departure = moment.tz("Europe/Prague");
                const departurePlain = stop.$.odj.split(":");
                departure.hour(parseInt(departurePlain[0], 10));
                departure.minute(parseInt(departurePlain[1], 10));
                departure.second(0);
                departure.millisecond(0);

                // midnight checking
                isOverMidnight = this.checkMidnight(now, departure); // returns -1, 1 or 0
                departure.add(isOverMidnight, "d");
            }

            // TEMP input data attribute rename - OLD: stop.$.zpoz_typ, NEW: stop.$.zpoz_typ_odj
            const delayDepartureType = stop.$.zpoz_typ || stop.$.zpoz_typ_odj;

            const cisStopSequence = i + 1;
            // finding cis_last_stop_sequence (positions table)
            if (
                res.position.cis_last_stop_id === parseInt(stop.$.zast, 10) &&
                ((delayDepartureType && parseInt(delayDepartureType, 10) === 3) ||
                    (stop.$.zpoz_typ_prij && parseInt(stop.$.zpoz_typ_prij, 10) === 3))
            ) {
                res.position.cis_last_stop_sequence = cisStopSequence;
            }

            // assign formatted stop id according to agency's stop ids
            const aswStopId = agencyDPPRAHAException ? this.formatASWStopId(stop.$.asw) : null;
            const cisStopId = !agencyDPPRAHAException ? parseInt(stop.$.zast, 10) : null;

            res.stops.push({
                arrival_delay_type: stop.$.zpoz_typ_prij ? parseInt(stop.$.zpoz_typ_prij, 10) : null,
                // @ts-ignore
                arrival_time: arrival ? stop.$.prij : null,
                // @ts-ignore
                arrival_timestamp: arrival ? arrival.utc().valueOf() : null,
                asw_stop_id: aswStopId,
                cis_stop_id: cisStopId,
                cis_stop_platform_code: stop.$.stan,
                cis_stop_sequence: cisStopSequence,
                delay_arrival: stop.$.zpoz_prij ? parseInt(stop.$.zpoz_prij, 10) : null,
                delay_departure: stop.$.zpoz_odj ? parseInt(stop.$.zpoz_odj, 10) : null,
                delay_type: delayDepartureType ? parseInt(delayDepartureType, 10) : null,
                // @ts-ignore
                departure_time: departure ? stop.$.odj : null,
                // @ts-ignore
                departure_timestamp: departure ? departure.utc().valueOf() : null,
                trips_id: primaryKey,
            });
        });

        return res as any;
    };

    /**
     * Returns -1 if start hour is 12-23 and now is 0-12, 1 if start hour is 18-23 and now 0-6, else 0
     *
     * @param {moment.Moment} now - Moment of position
     * @param {moment.Moment} start - Moment of start of trip
     * @returns {number}
     */
    private checkMidnight = (now: moment.Moment, start: moment.Moment): number => {
        // i.e. 0 - 22 = -22
        // -22 <= -12
        // trip starting in previous day, but never starts before 12:00
        if (now.hour() - start.hour() <= -(24 - 12)) {
            // "backwards" 12 hours
            return -1;
        }
        // i.e. 23 - 1 = +22
        // +22 >= +18
        // trip starting next day, sending positions early, not eariler than 6 hours before start
        else if (now.hour() - start.hour() >= 24 - 6) {
            // "forwards" 6 hours
            return 1;
        }
        return 0; // same day
    };

    /**
     * Fix source negative bearing value due to overflow by adding 256
     *
     * @param {number} bearing
     * @returns {number}
     */
    private fixSourceNegativeBearing(bearing: number): number {
        return bearing < 0 ? bearing + 256 : bearing;
    }

    /**
     * Format input stop id from DPP agency (XXX000Y to XXX/Y) to ASW id
     *
     * @param {string} stopId
     * @returns {stringList}
     */
    private formatASWStopId(stopId: string): string {
        const fixedRightPadFactor = 10000;
        const aswParsedStopNodeId = Math.floor(parseInt(stopId, 10) / fixedRightPadFactor);
        const aswParsedStopPostId = parseInt(stopId, 10) - aswParsedStopNodeId * fixedRightPadFactor;
        return aswParsedStopNodeId + "/" + aswParsedStopPostId;
    }
}
