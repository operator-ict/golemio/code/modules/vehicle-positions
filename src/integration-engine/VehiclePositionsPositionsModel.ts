import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { Validator } from "@golemio/core/dist/shared/golemio-validator";
import Sequelize, { QueryTypes } from "@golemio/core/dist/shared/sequelize";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { VehiclePositions } from "#sch/index";

export class VehiclePositionsPositionsModel extends PostgresModel implements IModel {
    /** Model name */
    public name!: string;
    /** The Sequelize Model */
    public sequelizeModel!: Sequelize.ModelCtor<any>;
    /** The Sequelize Model for temporary table */
    protected tmpSequelizeModel!: Sequelize.ModelCtor<any> | null;
    /** Validation helper */
    protected validator!: Validator;
    /** Type/Strategy of saving the data */
    protected savingType!: "insertOnly" | "insertOrUpdate";
    /** Associated vehiclepositions_trips Model for updating the delay */
    protected tripsModel: Sequelize.ModelCtor<any>;

    constructor() {
        super(
            VehiclePositions.positions.name + "Model",
            {
                outputSequelizeAttributes: VehiclePositions.positions.outputSequelizeAttributes,
                pgTableName: VehiclePositions.positions.pgTableName,
                savingType: "insertOnly",
                sequelizeAdditionalSettings: {
                    indexes: [
                        {
                            fields: ["trips_id"],
                            name: "vehiclepositions_positions_trips_id",
                        },
                        {
                            fields: ["origin_time"],
                            name: "vehiclepositions_positions_origin_time",
                        },
                    ],
                },
            },
            new Validator(
                VehiclePositions.positions.name + "ModelValidator",
                VehiclePositions.positions.outputMongooseSchemaObject
            )
        );
        this.tripsModel = PostgresConnector.getConnection().define(
            VehiclePositions.trips.pgTableName,
            VehiclePositions.trips.outputSequelizeAttributes,
            {}
        );
        this.tripsModel.hasMany(this.sequelizeModel, { foreignKey: "trips_id", sourceKey: "id" });
    }

    public getPositionsForUpdateDelay = async (tripIds: string[]): Promise<any> => {
        // TODO - check that origin_time is not duplicate for tracking == 2.
        // const originTimeColumn = `"vehiclepositions_positions"."origin_time"`;
        const results = await this.tripsModel.findAll({
            attributes: [
                // Sequelize.literal(`DISTINCT ON (${originTimeColumn}) ${originTimeColumn}`),
                "id",
                "gtfs_trip_id",
                "start_timestamp",
                "agency_name_scheduled",
                "start_cis_stop_id",
            ],
            include: [
                {
                    attributes: [
                        "lat",
                        "lng",
                        "origin_time",
                        "origin_timestamp",
                        "delay",
                        "tracking",
                        "id",
                        "shape_dist_traveled",
                    ],
                    model: this.sequelizeModel,
                    where: {},
                },
            ],
            // @ts-ignore
            order: [[{ model: this.sequelizeModel }, "origin_timestamp", "ASC"]],
            raw: true,
            where: {
                gtfs_trip_id: { [Sequelize.Op.ne]: null },
                id: { [Sequelize.Op.any]: Array.isArray(tripIds) ? tripIds : [tripIds] },
            },
        });

        // Sequlize return with raw==true flatten array of results, nest==true is available for Sequelize ver >5 only
        // We return objects of positions grouped by trips_id
        return results.reduce((p, c, i) => {
            let pIndex = p.findIndex((e: Record<string, any>) => e.trips_id === c.id);
            if (pIndex === -1) {
                p.push({
                    agency_name_scheduled: c.agency_name_scheduled,
                    gtfs_trip_id: c.gtfs_trip_id,
                    positions: [],
                    start_cis_stop_id: c.start_cis_stop_id,
                    start_timestamp: c.start_timestamp,
                    trips_id: c.id,
                });
                pIndex = p.findIndex((e: Record<string, any>) => e.trips_id === c.id);
            }
            p[pIndex].positions.push({
                delay: c["vehiclepositions_positions.delay"],
                id: c["vehiclepositions_positions.id"],
                lat: c["vehiclepositions_positions.lat"],
                lng: c["vehiclepositions_positions.lng"],
                origin_time: c["vehiclepositions_positions.origin_time"],
                origin_timestamp: c["vehiclepositions_positions.origin_timestamp"],
                shape_dist_traveled: c["vehiclepositions_positions.shape_dist_traveled"],
                tracking: c["vehiclepositions_positions.tracking"],
            });
            return p;
        }, []);
    };

    public bulkUpdate = async (data: any[]): Promise<any> => {
        const connection = PostgresConnector.getConnection();
        const transaction = await connection.transaction();

        try {
            const promises = data.map(async (d) => {
                const [res]: Array<{ exists: boolean }> = await connection.query(
                    `SELECT EXISTS(SELECT 1 FROM ${this.tableName} WHERE id='${d.id}') AS "exists";`,
                    {
                        transaction,
                        type: QueryTypes.SELECT,
                        raw: true,
                    }
                );
                const [record] = await this.sequelizeModel.upsert(d, { transaction });
                return { id: record.id, upd: !!res?.exists };
            });

            const records = await Promise.all(promises);
            await transaction.commit();

            return records.reduce(
                (acc, record) => {
                    if (record.upd) {
                        acc.updated.push(record.id);
                    }
                    return acc;
                },
                { updated: [] } as Record<"updated", any[]>
            );
        } catch (err) {
            await transaction.rollback();
            throw new CustomError("Error while saving to database.", true, this.name, 4001, err);
        }
    };
}
