import Sequelize from "@golemio/core/dist/shared/sequelize";
import { sequelizeConnection } from "@golemio/core/dist/output-gateway/database";
import { VehiclePositions } from "#sch/index";

export class VehiclePositionsVehicleTypesModel {
    /** The Sequelize Model */
    public sequelizeModel: Sequelize.ModelCtor<any>;

    public constructor() {
        this.sequelizeModel = sequelizeConnection.define(
            VehiclePositions.vehicleTypes.pgTableName,
            VehiclePositions.vehicleTypes.outputSequelizeAttributes
        );
    }

    public GetAll = async (): Promise<any> => {
        return null;
    };

    public GetOne = async (id: number): Promise<object | null> => {
        return null;
    };
}
